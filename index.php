<!-- 
*FCFS Algorithm
*Md Nasir Fardoush
*ID:201520612
 -->

<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
	<title>FCFS algorithm by Md Nasir Fardoush</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="wraper">
		<div>
			<fieldset>
				<legend>FCFS process inputs</legend>
				<p>Please enter another process events and burst time seperet by comma(,)</p>
				<p>Exam: P1,P2,P3 abd Burst time 10,5,20 </p>
				<?php if(isset($_SESSION['a_w_time'])){
							 echo "<h3>Avarage waiting time: ".$_SESSION['a_w_time']."</h3>"; 
							 unset($_SESSION['a_w_time']);
						}elseif(isset($_SESSION['error'])){
							echo "<h3>".$_SESSION['error']."</h3>"; 
						    unset($_SESSION['error']);
						}
					?>
				</h3>
				<form action="action.php" method="POST">
				<table>
					<tr>
						<td><label>Process Events</label></td>
						<td><input type="text" placeholder="P1,P2,P3...." name="process_events"></td>
					</tr>					
					<tr>
						<td><label>Burst Time</label></td>
						<td><input type="text" placeholder="10,20,5....." name="burst_time"></td>
					</tr>					
					<tr>
						<td></td>
						<td><input type="submit" value="GetAvg." name="subbmit"></td>
					</tr>
				</table>		


					
				</form>
			</fieldset>
		</div>
	</div>
</body>
</html>